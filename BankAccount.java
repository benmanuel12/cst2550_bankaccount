import java.util.concurrent.locks.*;

public class BankAccount
{
    private double balance;

    public BankAccount()
    {
        balance = 0;
    }

    private ReentrantLock myLock = new ReentrantLock();
    Condition positiveBalance = myLock.newCondition();

    public void deposit(double amount){
	myLock.lock();
        System.out.print("Depositing " + amount);
        double newBalance = balance + amount;
        System.out.println(", new balance is " + newBalance);
        balance = newBalance;
	myLock.unlock();
    }

    public void withdraw(double amount){
	try{
	    while (balance<amount){
		positiveBalance.await();
	    }
	    myLock.lock();
            System.out.print("Withdrawing " + amount);
            double newBalance = balance - amount;
            System.out.println(", new balance is " + newBalance);
            balance = newBalance;
	}
	catch (InterruptedException e) {
	    System.out.println("Interrupted");
	    
	}
	finally {
	    myLock.unlock();
	}	
    }

    public double getBalance(){
        return balance;
    }
}
